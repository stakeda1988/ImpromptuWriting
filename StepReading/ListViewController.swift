//
//  ListViewController.swift
//  ImpromptuWriting
//
//  Created by SHOKI TAKEDA on 12/6/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class ListViewController: UIViewController{
    
    var listArray:[[String]] = [["", ""], ["", ""]]
    var listLineArray:[String] = ["", ""]
    var listLine:String = ""
    var localEnglish:String = ""
    var localJapanese:String = ""
    
    @IBOutlet weak var listLabel: UILabel!
    
//    private var nadView: NADView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        // NADViewクラス
//        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
//        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
//            spotID: "475395")
//        nadView.isOutputLog = false
//        nadView.delegate = self
//        nadView.load()
//        self.view.addSubview(nadView)
        
        for i in 0...listArray.count-1 {
            localJapanese = listArray[i][0]
            localEnglish = listArray[i][1]
            listLine = listLine + "\r\r" + localJapanese + "\r" + localEnglish
        }
        
        listLabel.text = listLine
    }
}