//
//  ResultViewController.swift
//  StepReading
//
//  Created by SHOKI TAKEDA on 11/3/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController{
    
    @IBOutlet weak var japaneseLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var englishLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var topicCounter: UILabel!
//    private var nadView: NADView!
    var rowArray:[[String]] = [["",""], ["",""]]
    var counter10:Int = 0
    var restCounter:Int = 15
    var rest10Counter:Int = 10
    @IBOutlet weak var showCountSecond: UILabel!
    var timer:NSTimer!
    var startCount = true
    var counter:Int = 0
    @IBAction func pauseButton(sender: UIButton) {
        timer.invalidate()
        startButton.hidden = false
        pauseLabel.hidden = true
    }
    @IBOutlet weak var pauseLabel: UIButton!
    @IBAction func saveLearning(sender: UIButton) {
//        performSegueWithIdentifier("shift",sender: nil)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "shift" {
            let taskController:ListViewController = segue.destinationViewController as! ListViewController
            taskController.listArray = rowArray
        }
    }
    @IBAction func startSentences(sender: UIButton) {
        japaneseLabel.hidden = false
//        japaneseLabel.text = rowArray[0][0]
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
        startButton.hidden = true
        pauseLabel.hidden = false
    }
    
    func showText(_counter: Int){
        restCounter = 15 - _counter % 15
        if restCounter > 5 && restCounter <= 15 {
            rest10Counter = restCounter - 5
            showCountSecond.text = String(rest10Counter)
            timeSlider.minimumValue = 1
            timeSlider.maximumValue = 10
            timeSlider.value = Float(rest10Counter)
            for i in 0...rowArray.count {
                if i == counter10 {
                    japaneseLabel.hidden = false
                    englishLabel.hidden = true
                    japaneseLabel.text = rowArray[i][0]
                }
            }
        } else if restCounter == 6 {
            rest10Counter = restCounter - 5
            showCountSecond.text = String(rest10Counter)
            timeSlider.minimumValue = 1
            timeSlider.maximumValue = 10
            timeSlider.value = Float(rest10Counter)
            for i in 0...rowArray.count {
                if i == counter10 {
                    japaneseLabel.hidden = false
                    englishLabel.hidden = true
                    japaneseLabel.text = rowArray[i][0]
                }
            }
        } else if  restCounter < 6 && restCounter > 1 {
            showCountSecond.text = String(restCounter)
            timeSlider.minimumValue = 1
            timeSlider.maximumValue = 5
            timeSlider.value = Float(restCounter)
            for i in 0...rowArray.count {
                if i == counter10 {
                    japaneseLabel.hidden = false
                    japaneseLabel.text = rowArray[i][0]
                    englishLabel.hidden = false
                    englishLabel.text = rowArray[i][1]
                }
            }
        } else if restCounter == 1 {
            showCountSecond.text = String(restCounter)
            timeSlider.minimumValue = 1
            timeSlider.maximumValue = 5
            timeSlider.value = Float(restCounter)
            for i in 0...rowArray.count {
                if i == counter10 {
                    japaneseLabel.hidden = false
                    japaneseLabel.text = rowArray[i][0]
                    englishLabel.hidden = false
                    englishLabel.text = rowArray[i][1]
                }
            }
            counter10++
        }
    }
    func onUpdate(){
        topicCounter.text = String(counter10+1) + "/" + String(rowArray.count)
        counter += 1
        showText(counter)
        if counter10 == rowArray.count {
            timer.invalidate()
            timeSlider.value = 0
            saveButton.hidden = false
            pauseLabel.hidden = true
        }
    }
    @IBAction func backToPage(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var japaneseLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        // NADViewクラス
//        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
//        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
//            spotID: "475395")
//        nadView.isOutputLog = false
//        nadView.delegate = self
//        nadView.load()
//        self.view.addSubview(nadView)
        
        japaneseLabelWidth.constant = myBoundSize.width-20
        englishLabelWidth.constant = myBoundSize.width-20
        
        saveButton.hidden = true
        pauseLabel.hidden = true
        englishLabel.hidden = true
        japaneseLabel.hidden = true
        englishLabel.hidden = true
        japaneseLabel.hidden = true
        showCountSecond.text = "10"
        showCountSecond.hidden = true
        timeSlider.minimumValue = 1
        timeSlider.maximumValue = 10
        timeSlider.value = 10
        topicCounter.text = "1/" + String(rowArray.count)
    }
}
